本项目是SpringBoot的一个简单的入门项目,做了一个关于图书的CRUD操作<br/>
1. 使用MongoDB数据库<br/>
2. yml配置<br/>
3. SpringMVC<br/>
4. 全注解<br/>
5. undertow内嵌服务器<br/>
6. 页面使用Thymeleaf模板<br/>

Book相关的使用了Redis缓存,如果没装Redis,可以自行删除Redis相关的代码.<br/>

若只是想看看Hello的内容的话,直接访问HelloController的内容即可<br/>
package com.hello.service;

import com.hello.domain.Book;
import com.hello.repository.BookRepository;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * @author : zga
 * @date : 2016-3-21
 */
@Service
public class BookService {

    @Resource
    BookRepository bookRepository;

    @Resource
    MongoTemplate mongoTemplate;

    /*@Autowired
    RedisTemplate redisTemplate;*/

    /**
     * @author: zga
     * @function: add new book
     * @param book
     * @return
     */
    public Integer addNewBook(Book book) {
        try {
            mongoTemplate.save(book);
            return 1;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * @autho: zga
     * @function: delete book by isbn
     * @param book
     * @return
     */
    public Integer delBook(Book book) {
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.and("isbn").is(book.getIsbn());
        query.addCriteria(criteria);
        mongoTemplate.remove(query,"book");
        return null;
    }

    /**
     * @autho: zga
     * @function: update book info by isbn
     * @param book
     * @return
     */
    public Integer updateBook(Book book) {
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.and("isbn").is(book.getIsbn());
        query.addCriteria(criteria);

        Update update = new Update();
        update.set("price",book.getPrice());
        update.set("expired",book.getExpired());

        mongoTemplate.updateFirst(query,update,Book.class,"book");
        return null;
    }

    /**
     * @author: zga
     * @function: find all books
     * @return
     */
    public List selectAllBook() {
        List bookList = getBookListFromCache();
        if(bookList == null){
            bookList = new ArrayList<>();
            Iterable<Book> iterable = bookRepository.findAll();
            Iterator<Book> iterator = iterable.iterator();
            while (iterator.hasNext()) {
                bookList.add(iterator.next());
            }
            /*redisTemplate.opsForList().rightPush("bookList", bookList);*/
        } else{
            List list = Arrays.asList(bookList.toArray());
            return list;
        }
        return bookList;
    }

    /**
     * @author: zga
     * @function: find book by isbn
     * @param isbn
     * @return
     */
    public Book selectBookByIsbn(String isbn) {
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.and("isbn").is(isbn);

        query.addCriteria(criteria);

        Book book = mongoTemplate.findOne(query,Book.class);

        return book;
    }

    /**
     * @author: zga
     * @function: 获取Redis缓存中的bookList内容.
     * @return
     */
    public List getBookListFromCache(){
       /* long flag = redisTemplate.opsForSet().size("bookList");
        if(flag > 0){
            Set set = redisTemplate.opsForSet().members("bookList");
            List list = new ArrayList<>();

            Iterator iterator = set.iterator();
            while (iterator.hasNext()) {
                list.add(iterator.next());
            }
            return list;
        }*/
        return null;
    }

}

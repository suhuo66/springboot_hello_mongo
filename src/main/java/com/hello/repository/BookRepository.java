package com.hello.repository;

import com.hello.domain.Book;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author : zga
 * @date : 2016-3-21
 */
@Repository
public interface BookRepository  extends CrudRepository<Book,String>{

}

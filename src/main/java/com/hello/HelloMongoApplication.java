package com.hello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author : zga
 * @date : 2016-3-21
 */
@SpringBootApplication
public class HelloMongoApplication {

    public static void main(String[] args) {
        SpringApplication.run(HelloMongoApplication.class, args);
    }
}
